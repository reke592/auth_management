let models = require('./models')
let Op = models.Sequelize.Op
let services = require('./services')
let _ = require('lodash')

function attr(model) {
  let tmp = models[model]
  if(!tmp) return
  Object.getOwnPropertyNames(tmp.__proto__)
    .map(attr => {
      console.log(`model: ${model}.${attr}`)
    })
  Object.getOwnPropertyNames(tmp.build())
    .map(attr => {
      console.log(`bulid: ${model}.${attr}`)
    })
  Object.getOwnPropertyNames(tmp.build().__proto__)
  .map(attr => {
    console.log(`proto: ${model}.${attr}`)
  })
  console.log('---')
}

// attr('Claim')
// attr('Claim')
// attr('ResourceOwner')
// attr('ResourceModel')

// let q = { 
//   where: {
//     user_id: {
//       [Op.eq]: 2
//     }
//   },
//   attributes: ['permission'],
//   include: [
//     {
//       model: models.ResourceModel,
//       as: 'model',
//       attributes: ['name']
//     }
//   ]
// }

// models.Claim.findAll(q)
//   .then(r => {
//     console.log(r.length)
//   })

let claims = [
  {
    res_model_id: 1,
    permission: 0
  },
  {
    res_model_id: 2,
    permission: 'a'
  },
  {
    res_model_id: 1,
    permission: 1
  },
  {
    res_model_id: 2,
    permission: 4
  },
  {
    res_model_id: 1,
    permission: 8
  }
]

let no_grant = []
x = _.filter(claims, function ({ res_model_id, permission }) {
  if(res_model_id > 0 && permission >= 0 && permission <= 15) return true
  //else
  no_grant.push({ res_model_id, permission })
  return false
})

r = _.reduce(x, (result, { res_model_id, permission }) => {
  result[res_model_id] = (result[res_model_id] || 0) | permission
  // result[res_model_id] = result[res_model_id] | permission
  return result
}, {})
// console.log(_)

console.log(_.zipObject(['res_model_id', 'permission'], [ Object.keys(r) ]))