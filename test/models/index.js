var assert = require('assert')
var models = require('../../models')
var Op = models.Sequelize.Op

function importTest(name, path) {
  describe(name, function() {
    require(path)({ models, Op, assert })
  })
}

describe('Test: Auth-Server', function () {
  before(function () {
    return models.sequelize.sync({ force: true, match: /_test/ })
  })

  describe('Models', function () {
    importTest('User', './user')
    importTest('Claim', './claim')
  })
})
