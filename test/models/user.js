module.exports = function ({ models, Op, assert }) {

  it('should not create user without password', function (done) {
    models.User.create({ username: 'test' })
      .catch(err => {
        done();
      })
  })

  it('should not create user without username', function (done) {
    models.User.create({ password: 'test' })
      .catch(err => {
        done();
      })
  })

  it('should throw an error when username length is below 4 characters', function (done) {
    models.User.create({ username: 'tes', password: 'test1234' })
      .catch(err => {
        done();
      })    
  })

  it('should throw an error when username length is above 20 characters', function (done) {
    models.User.create({ username: '01234_01234_01234_01234', password: 'test1234' })
      .catch(err => {
        done();
      })    
  })

  it('should throw an error when password length is below 8 characters', function (done) {
    models.User.create({ username: 'test1234', password: 'test' })
      .catch(err => {
        done();
      })    
  })

  it('should throw an error when password length is above 30 characters', function (done) {
    models.User.create({ username: 'test', password: '0987654321_0987654321_0987654321' })
      .catch(err => {
        done();
      })    
  })

  it('should create the user with encrypted password', async function () {
    let user = await models.User.create({ username: 'test1234', password: 'test1234' });
    assert.notEqual('test', user.password);
  });

}
