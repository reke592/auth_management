module.exports = function ({ models, Op, assert }) {
  
  it('should throw an error when fields [ res_model_id, user_id ] is null or empty', function (done) {
    models.Claim.create({ permission: 1 })
      .catch(err => {
        done();
      })
  })

  it('should create claim even if permission is not defined, default is 0 (NO ACCESS)', async function () {
    let user = await models.User.create({ username: 'testclaim_create', password: 'testclaim_create' })
    let model = await models.ResourceModel.create({ name: 'TestModel1' })
    let claim1 = await models.Claim.create({ res_model_id: model.id, user_id: user.id })
    let claim2 = await models.Claim.create({ res_model_id: model.id, user_id: user.id, permission: 5 })
    assert.equal(claim1.permission, 0);
    assert.equal(claim2.permission, 5);
  })

  it('should have an instance method "Claim.destroy" to delete the record / mark as deleted', async function () {
    let user = await models.User.create({ username: 'testclaim_delete', password: 'testclaim_destroy' })
    let model = await models.ResourceModel.create({ name: 'TestModel2' })
    let claim = await models.Claim.create({ res_model_id: model.id, user_id: user.id })
    assert.equal(toString.call(claim.destroy), '[object Function]')
    return claim.destroy()
  })

}
