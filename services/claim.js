const models = require('../models')
const _ = require('lodash')
const Op = models.Sequelize.Op

function validate (value) {
  if(value.constructor != Array)
    value = [value]
  
  let err = []
  // isolate invalid claims
  let hdl_filter = function ({res_model_id, permission }) {
    if(res_model_id > 0 && permission >= 0 && permission <= 15) return true
    err.push({ res_model_id, permission })
    return false
  }

  // combine claims with same res_model_id
  let hdl_reduce = function (result, { res_model_id, permission }) {
    result[res_model_id] = (result[res_model_id] || 0) | permission
    return result
  }
  // result: [ { claim_id: combined_permissions }, ... ]

  let hdl_map = function (permission, res_model_id) {
    return { permission, res_model_id }
  }

  let result = _.filter(value, hdl_filter)

  if(err.length) return Promise.reject({
    error: 'requires only numeric value',
    claims: err
  })

  result = _.reduce(result, hdl_reduce, {})
  result = _.map(result, hdl_map)

  return Promise.resolve(result)
}

// exports.create = function (user_id, claims) {
//   // convert reduced validate result to sequelize format for model.bulkCreate
//   let hdl_map = function(permission, res_model_id) {
//     return { permission, res_model_id, user_id }
//   }

//   return validate(claims)
//     .then(result => {
//       return models.Claim.bulkCreate(_.map(result, hdl_map))
//     })
// }

exports.createOrUpdate = function (t_user_id, claims) {
  return validate(claims)
    .then(params => {

      let results = _.map(params, each => {
        let q = {
          attributes: ['id'],
          where: {
            user_id: {
              [Op.eq]: t_user_id
            },
            res_model_id: {
              [Op.eq]: each.res_model_id
            }
          },
          defaults: {
            user_id: t_user_id,
            res_model_id: each.res_model_id,
            permission: each.permission
          },
          include: [
            {
              model: models.ResourceModel,
              as: 'model',
              attributes: ['name']
            }
          ]
        }

        return models.Claim
          .findOrCreate(q)
          .spread((record, is_created) => {
            // for existing record
            if(!is_created) {
              return record.update({ permission: each.permission })
                .then(claim => {
                  return {
                    claim,
                    status: 'updated'
                  }
                })
            // for newly created record
            } else {
              return {
                claim: record,
                status: 'created'
              }
            }
          })
          .catch(err => {
            let message = 'unknown error'
            if(err.message.match("foreign key constraint"))
              message = 'record not found'
            return {
              claim: each,
              status: 'error',
              message
            }
          })
      }) // map each

      return Promise.all(results)
    }) // validate
}

exports.remove = function (t_user_id, claims) {
  return validate(claims)
    .then(params => {
      let id_list = _.map(params, 'res_model_id');
      let expr = (id_list.length > 1) ? Op.in : Op.eq;

      let q = {
        where: {
          user_id: {
            [Op.eq]: t_user_id
          },
          res_model_id: {
            [expr]: id_list
          }
        }
      }
      return models.Claim.destroy(q)
        .then(deleted => {
          return { deleted }
        })
    })
}

exports.userClaims = function (t_user_id) {
  let query = {
    where: {
      user_id: {
        [Op.eq]: t_user_id
      }
    },
    attributes: ['permission'],
    include: [
      {
        model: models.ResourceModel,
        as: 'model',
        attributes: ['name']
      }
    ]
  }
  
  return models.Claim.findAll(query)
}
