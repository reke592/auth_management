const models = require('../models')
const Op = models.Sequelize.Op
const bcrypt = require('bcrypt')
const services = {
  claim: require('./claim')
}

exports.create = function ({ username, password }) {
  if(!username || !password)
    return Promise.reject(new Error('required parameters [ username, password ]'))
  
  return models.User.create({ username, password })
}

exports.getClaims = function (user_id) {
  if(!user_id || !Number.parseInt(user_id))
    return Promise.reject(new Error('invalid user_id: ' + user_id))

  return services.claim.userClaims(user_id)
}

exports.addClaims = function (user_id, claims) {
  if(claims.constructor != Array)
    claims = [claims]

  let q_user = {
    where: {
      id: {
        [Op.eq]: user_id
      }
    }
  }
  return models.User
    .find(q_user)
    .then(record => {
      if(!record) throw new Error('user not found') 
      return services.claim.create(record.id, claims)
    })
}

exports.removeClaims = function (user_id, claim_ids) {
  return _claims(user_id, claim_ids, 'remove')
}

exports.updateClaims = function (user_id, claims, strategy) {
  if(claims.constructor != Array)
    claims = [claims]

  return models.User
    .findById(user_id)
    .then(record => {
      if(!record) throw new Error('user not found')
      if(strategy == 'update')
        return services.claim.createOrUpdate(user_id, claims)
      else if(strategy == 'remove')
        return services.claim.remove(user_id, claims)
      else
        throw new Error('invalid patch strategy:', strategy)
    })
}

// exports.updateClaims = async function _claims (user_id, claim_ids, strategy) {
//   if(claim_ids.constructor !== Array)
//     claim_ids = [ claim_ids ]

//   try {
//     let q_user = { where: { id: { [Op.eq]: user_id } } }
//     let q_claim = { where: { id: { [Op.in]: claim_ids } } }
//     let user = await models.User.find(q_user)
//     let claims = await models.Claim.findAll(q_claim)

//     // reject
//     if(!user) {
//       throw new Error('could not update claims, user not exist')

//     // resolve
//     } else {
//       if(strategy == 'add') {
//         user.removeClaims(claims) // remove previous grant
//         user.addClaims(claims)    
//       }
//       else if(strategy == 'remove')
//         user.removeClaims(claims)
//       else 
//         throw new Error('could not update claims, invalid update strategy')

//       return user.save()
//         .then(record => services.claim.userClaims(record.id))
//     }
//   //reject
//   } catch (err) {
//     if(process.env.NODE_ENV == 'production')
//       throw new Error('could not process the request')
//     else
//       throw err
//   }
// }

exports.signIn = function (login, params) {
  if(login === 'password')
    return _usePassword(params)
  else
    return Promise.reject(new Error('invalid login strategy'))
}

async function _usePassword (params) {
  let q = {
    where: {
      username: {
        [Op.eq]: params.username
      }
    }
  }

  try {
    let user = await models.User.find(q)
    if(!user) throw new Error('user not exist')

    let result = await bcrypt.compare(params.password, user.password)
    if(!result) throw new Error('invalid password')

    return {
      user_id: user.id,
      username: user.username,
      result
    }
  } catch(err) {
    if(process.env.NODE_ENV == 'production')
      throw new Error('could not process the request')
    else
      throw err;
  }
}
