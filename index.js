const Express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

const app = new Express()
const routes = require('./router')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cookieParser())
app.use('/api/v1', routes)

app.listen(process.env.NODE_PORT)
console.log('Auth server listening on port', process.env.NODE_PORT)
