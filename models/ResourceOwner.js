'use strict';
module.exports = (sequelize, DataTypes) => {
  var attributes = {
    protocol: {
      type: DataTypes.STRING(5),
      allowNull: false
    },
    host: {
      type: DataTypes.STRING,
      allowNull: false
    },
    port: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    // resource path where the authorization header will be sent
    // this path will also return the access_token to the agent
    end_point: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }

  var options = {
    tableName: 'resource_owners',
    underscored: true,
    paranoid: true,
    getterMethods: {
      address: function() {
        return `${this.protocol}://${this.host}:${this.port}/${this.resource}`
      }
    }
  }

  var ResourceOwner = sequelize.define('ResourceOwner', attributes, options);
  ResourceOwner.associate = function(models) {
    // associations can be defined here
    ResourceOwner.hasMany(models.ResourceModel, { foreignKey: 'res_owner_id' })
  };
  return ResourceOwner;
};