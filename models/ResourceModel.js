'use strict';
module.exports = (sequelize, DataTypes) => {

  var attributes = {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }

  var options = {
    tableName: 'resource_models',
    underscored: true,
    paranoid: true
  }

  var ResourceModel = sequelize.define('ResourceModel', attributes, options);
  ResourceModel.associate = function(models) {
    // associations can be defined here
    ResourceModel.belongsTo(models.ResourceOwner, { foreignKey: 'res_owner_id' })
  };
  return ResourceModel;
};