'use strict';

const bcrypt = require('bcrypt');
const saltRounds = 12;

function hashPassword(instance) {
  if(!instance.changed('password')) return;
  return bcrypt.hash(instance.password, saltRounds)
    .then(hash => {
      instance.password = hash;
    })
}

module.exports = (sequelize, DataTypes) => {
  var attributes = {
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true, 
      validate: {
        len: {
          args: [4, 20],
          msg: 'minimum length for username is 4-20 characters'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: {
          args: [8, 30],
          msg: 'invalid password length, minimum is 8 characters'
        }
      }
    }
  }

  var options = {
    tableName: 'users',
    underscored: true,

    // add deleted field
    paranoid: true,

    hooks: {
      beforeCreate: hashPassword,
      beforeUpdate: hashPassword
    }
  }

  var User = sequelize.define('User', attributes, options);
  User.associate = function(models) {
    // associations can be defined here
    // User.belongsToMany(models.Claim, { through: 'user_claims', foreignKey: 'user_id', other_key: 'claim_id' })
    User.hasMany(models.Claim, { foreignKey: 'user_id' })
  };

  return User;
};
