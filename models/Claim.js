'use strict';
module.exports = (sequelize, DataTypes) => {

  var attributes = {
    permission: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0
    },
    res_model_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }

  var options = {
    tableName: 'claims',
    underscored: true,
    name: {
      singular: 'claim',
      plural: 'claims'
    }
  }

  var Claim = sequelize.define('Claim', attributes, options);
  Claim.associate = function(models) {
    // associations can be defined here
    Claim.belongsTo(models.ResourceModel, { as: 'model', foreignKey: 'res_model_id' })
  };
  return Claim;
};