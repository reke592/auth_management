const routes = require('express').Router()
const controllers = require('./controllers')

routes.post('/login', controllers.UserController.postLogin)
routes.patch('/:user_id/claims/update', controllers.UserController.patchClaims)
routes.get('/:user_id/claims', controllers.UserController.getClaims)

module.exports = routes
