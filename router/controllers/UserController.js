const services = require('../../services')

exports.postLogin = function (req, res) {
  services.user.signIn(req.body.login, req.body)
    .then(result => {
      res.send(result)
    })
    .catch(err => {
      res.send({ error: err.message })
    })
}

exports.patchClaims = function (req, res) {
  let {
    claim,
    claims,
    patch
  } = req.body

  services.user.updateClaims(req.params.user_id, (claim || claims), patch)
    .then(result => {
      res.json(result)
    })
    .catch(err => {
      res.json(err)
    })
}

exports.getClaims = function (req, res) {
  services.claim.userClaims(req.params.user_id)
    .then(claims => {
      res.json(claims)
    })
    .catch(err => {
      res.json({ error: err.message })
    })
}
